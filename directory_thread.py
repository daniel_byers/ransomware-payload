import threading, os

class directoryThread(threading.Thread):
  """docstring for directoryThread"""

  def __init__(self, queue, logger, group=None, target=None, name=None, args=(), kwargs=None):
    threading.Thread.__init__(self, group=group, target=target, name=name)
    self.queue = queue
    self.logger = logger
    self.args = args
    self.whitelist = ['.txtt']

  def run(self):
    env, admin = self.args
    dirs = collectDirectories().collect(env, admin, self.whitelist)
    # Add each file to the registry key
    file = open('reg_file.reg', 'w')
    for _dir in dirs:
      file.write('%s\n' % _dir)
    file.close()
    self.logger.debug('directory list built')

class collectDirectories(object):
  def collect(self, env, admin, whitelist):
    directory_array = []
    if(env == 'windows'):
      root = 'C:/' if admin else 'C:/Users/'
    else:
      root = '/' if admin else '/home/dann/workspace/python/assignment/filesystem/' 
    for dirname, dirnames, filenames in os.walk(root):
      for filename in filenames:
        name, ext = os.path.splitext(filename)
        if ext in whitelist:
          directory_array.append(os.path.join(dirname, filename))
    return directory_array