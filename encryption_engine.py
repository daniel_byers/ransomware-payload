import random, cryptomath, math

class encryptionEngine(object):
  def encrypt_affine(self, key, message):
    symbols = """abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~"""
    keyA = key // len(symbols)
    keyB = key % len(symbols)
    ciphertext = ''
    for symbol in message:
      if symbol in symbols:
        symIndex = symbols.find(symbol)
        ciphertext += symbols[(symIndex * keyA + keyB) % len(symbols)]
      else:
        ciphertext += symbol
    return ciphertext

  def encrypt_transposition(self, key, message):
    ciphertext = [''] * key
    for col in range(key):
      pointer = col
      while pointer < len(message):
        ciphertext[col] += message[pointer]
        pointer += key
    return ''.join(ciphertext)

  def encrypt_vigenere(self, key, message):
    letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    translated = []
    keyIndex = 0
    key = key.upper()
    for symbol in message:
      num = letters.find(symbol.upper())
      if num != -1:
        num += letters.find(key[keyIndex])
        num %= len(letters)
        if symbol.isupper():
          translated.append(letters[num])
        elif symbol.islower():
          translated.append(letters[num].lower())
        keyIndex += 1
        if keyIndex == len(key):
          keyIndex = 0
      else:
        translated.append(symbol)
    return ''.join(translated)

  def encrypt(self, keys, message, counter = 16):
    if counter != 0:
      message = self.encrypt(keys, message, counter - 1)
    # print('%s: %s' % (counter, message)) # Prints each recursion level
    return self.encrypt_vigenere(keys['vigenere'], self.encrypt_transposition(keys['transposition'], self.encrypt_affine(keys['affine'], message)))
  